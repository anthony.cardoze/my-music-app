export const environment = {
  production: true,
  client_id: `a2c0b71bfd044986900f1a8b517fb26a`,
  redirect_uri: `https://my-music-app-omega.vercel.app/login`,
};
